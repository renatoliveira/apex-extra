public without sharing class IntegerListFromRecords {
    public List<Integer> data { public get; private set; }

    public IntegerListFromRecords(
        List<SObject> records,
        Schema.SObjectField field
    ) {
        this.data = new List<Integer>();

        for (SObject record : records) {
            this.data.add((Integer) record.get(field));
        }
    }
}