@IsTest
private class ExtraMethodsTests {
    @IsTest
    private static void test() {
        // Given
        List<Account> accounts = new List<Account>{
            new Account(
                Name = 'Test',
                NumberOfEmployees = 100,
                AnnualRevenue = 1.1
            ),
            new Account(
                Name = 'Test',
                NumberOfEmployees = 100,
                AnnualRevenue = 1.1
            )
        };

        List<Contact> contacts = new List<Contact>{
            new Contact(
                FirstName = 'John',
                Birthdate = Date.newInstance(1, 1, 1990)
            ),
            new Contact(
                FirstName = 'Mary',
                Birthdate = Date.newInstance(1, 1, 1990)
            )
        };

        // When
        Test.startTest();
        List<Integer> integers = Extra.integersFromRecordList(
            accounts,
            Schema.Account.NumberOfEmployees
        );
        List<String> strings = Extra.stringsFromRecordList(
            accounts,
            Schema.Account.Name
        );
        List<Decimal> decimals = Extra.decimalsFromRecordList(
            accounts,
            Schema.Account.AnnualRevenue
        );
        List<Date> dates = Extra.datesFromRecordList(
            contacts,
            Schema.Contact.Birthdate
        );
        Test.stopTest();

        // Then
        System.assertEquals(
            2,
            integers.size(),
            'Should have computed two objects to the list.'
        );
        System.assertEquals(
            2,
            strings.size(),
            'Should have computed two objects to the list.'
        );
        System.assertEquals(
            2,
            decimals.size(),
            'Should have computed two objects to the list.'
        );
        System.assertEquals(
            2,
            dates.size(),
            'Should have computed two objects to the list.'
        );

        assertNoNullElements(integers);
        assertNoNullElements(strings);
        assertNoNullElements(decimals);
        assertNoNullElements(dates);
    }

    private static void assertNoNullElements(List<Object> items) {
        for (Object item : items) {
            System.assert(item != null, 'Item should not be null.');
        }
    }
}