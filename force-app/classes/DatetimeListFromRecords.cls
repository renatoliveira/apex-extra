public without sharing class DatetimeListFromRecords {
    public List<Datetime> data { public get; private set; }

    public DatetimeListFromRecords(
        List<SObject> records,
        Schema.SObjectField field
    ) {
        this.data = new List<Datetime>();

        for (SObject record : records) {
            this.data.add((Datetime) record.get(field));
        }
    }
}