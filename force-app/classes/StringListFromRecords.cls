public without sharing class StringListFromRecords {
    public List<String> data { public get; private set; }

    public StringListFromRecords(
        List<SObject> records,
        Schema.SObjectField field
    ) {
        this.data = new List<String>();

        for (SObject record : records) {
            this.data.add((String) record.get(field));
        }
    }
}