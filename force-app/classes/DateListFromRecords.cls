public without sharing class DateListFromRecords {
    public List<Date> data { public get; private set; }

    public DateListFromRecords(
        List<SObject> records,
        Schema.SObjectField field
    ) {
        this.data = new List<Date>();

        for (SObject record : records) {
            this.data.add((Date) record.get(field));
        }
    }
}