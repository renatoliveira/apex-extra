public without sharing class Extra {
    public static List<Integer> integersFromRecordList(
        List<SObject> records,
        Schema.SObjectField field
    ) {
        return new IntegerListFromRecords(records, field).data;
    }

    public static List<String> stringsFromRecordList(
        List<SObject> records,
        Schema.SObjectField field
    ) {
        return new StringListFromRecords(records, field).data;
    }

    public static List<Decimal> decimalsFromRecordList(
        List<SObject> records,
        Schema.SObjectField field
    ) {
        return new DecimalListFromRecords(records, field).data;
    }

    public static List<Date> datesFromRecordList(
        List<SObject> records,
        Schema.SObjectField field
    ) {
        return new DateListFromRecords(records, field).data;
    }

    public static List<Datetime> dateTimesFromRecordList(
        List<SObject> records,
        Schema.SObjectField field
    ) {
        return new DatetimeListFromRecords(records, field).data;
    }
}