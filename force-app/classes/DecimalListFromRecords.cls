public without sharing class DecimalListFromRecords {
    public List<Decimal> data { public get; private set; }

    public DecimalListFromRecords(
        List<SObject> records,
        Schema.SObjectField field
    ) {
        this.data = new List<Decimal>();

        for (SObject record : records) {
            this.data.add((Decimal) record.get(field));
        }
    }
}